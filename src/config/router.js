import Vue from 'vue'
import VueRouter from 'vue-router'
import DashboardUser from '../views/DashboardUser'

Vue.use(VueRouter)

const routes = [
    {
        path: '/dashboard',
        name: 'dashboardUser',
        component: DashboardUser
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
