import fetch from '../config/fetch'

export const checkTokenFetch = async (token) => {

    const body = {
        token: token
    }

    return await fetch(process.env.VUE_APP_API_URL + '/confirm_token', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(res => {
        if (res != null) {
            return res
        }
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}
