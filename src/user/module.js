import { checkTokenFetch } from './service'

const user = {
    id: null,
    username: '',
    email: '',
    roles: null,
    token: null,
    isConnected: 'pending'
}

const mutations = {
    CHECK_TOKEN: (state, res) => {
        if(res.isValid) {
            const user = res.user;
            console.log(state);
            state.user.id = user.id;
            state.user.email = user.email;
            state.user.username = user.username;
            state.user.token = user.token;
            state.user.isConnected = true;
            localStorage.setItem('user-token', user.token)
        } else {
            localStorage.removeItem('user-token')
            state.user.isConnected = false;
        }
    }
}

const actions = {
    checkToken: async (store, token) => {
        const res = await checkTokenFetch(token)
        store.commit('CHECK_TOKEN', res)
    },
    noToken: (store) => {
        store.commit('CHECK_TOKEN', {isValid: false})
    }
}
const getters = {
    user: state => state.user
}

export default {
    state: () => ({ user }),
    mutations: mutations,
    actions: actions,
    getters: getters,
}